const mongo = require('mongoskin');
const url = require('url');
const config = require('../../config/config.js');

const mongoUri = url.format({
    protocol: 'mongodb',
    slashes: true,
    hostname: 'nodejs_app_mongo',
    port: '27017',
    pathname: config.db.name
});

module.exports = mongo.db(mongoUri, {native_parser: true});
