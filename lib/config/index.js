const configValidator = require('./config-validator');
const config = require('../../config/config.js');

configValidator.validate(config);

module.exports = config;
