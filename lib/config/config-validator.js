let assertIsSet = (variable, message) => {
    if (undefined === variable) {
        console.error('Configuration error: ' + message);

        return process.exit(1);
    }
};

module.exports = {
    validate: function(config) {
        assertIsSet(config.db.name, 'Database name is undefined');
    }   
};
