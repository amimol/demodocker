const winston = require('winston');
const expressWinston = require('express-winston');
const path = require('path');

const requestLogger = expressWinston.logger({
  transports: [ new winston.transports.File({ filename: path.join('/var/log/nodejs', 'access.log') }) ]
});

const errorLogger = expressWinston.errorLogger({
  transports: [ new winston.transports.File({ filename: path.join('/var/log/nodejs', 'error.log') }) ],
  dumpExceptions: true,
  showStack: true
});

const appLogger = new (winston.Logger)({
  transports: [ new (winston.transports.File)({ filename: path.join('/var/log/nodejs', 'app.log') }) ]
});

module.exports = {
  requestLogger,
  errorLogger,
  appLogger
};
