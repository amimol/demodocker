const express = require('express');
const exphbs = require('express-handlebars');
const bodyParser= require('body-parser');
const requestLogger = require('./logger').requestLogger;

let app = express();

app.engine('.hbs', exphbs({extname: '.hbs'}));

app.set('view engine', '.hbs');

app.use(express.static('web'));
app.use(bodyParser.urlencoded({extended: true}));
app.use(requestLogger);

module.exports = app;
