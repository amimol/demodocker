# Docker Template

## PHP Application (Symfony)

[PHP Symfony - http://localhost/](http://localhost/)

## NodeJs Application

[NodeJs Application - http://localhost:3000/](http://localhost:3000/)

## Java Application

[Java Application - http://localhost:8000/](http://localhost:8000/)

## MySql

[localhost:3306](localhost:3306)

## Mailcatcher

[Mailcatcher - http://localhost:1080/](http://localhost:1080/)

## Redis 

[localhost:6379](localhost:6379)

## ELK 

TODO: Mount ES data directory, so data will not be lost after container restart.

### Kibana

[Kibana - http://localhost:5601/](http://localhost:5601/)

#### Setting up main indice

* Go to "Settings > Indicies".
* "Configure an index pattern" put `filebeat-*`
* "Unable to fetch mapping. Do you have indices matching the pattern?" should change to green "Create"
* Submit with "Create"

#### Import searches

* Go to "Settings > Objects"
* Import `ELK/kibana/export.json` file with "Import" button

### Filebeat
Logs from services need to be mounted in `Filebeat/logs`.
Configuration of shipped logs is in `Filebeat/configs/filebeat.yml` config file.

### ElasticSearch

    localhost:9200

For example:

    curl 'localhost:9200/_cat/indices?v'