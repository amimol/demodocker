FROM node

EXPOSE 3000

RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app


COPY ./ /usr/src/app

RUN chmod 755 /usr/src/app/*.sh
RUN mkdir /var/log/nodejs

CMD ["/usr/src/app//entrypoint.sh"]
